# Operacijski sustavi - Vjezbe

## Materijali za vjezbe iz predmeta operacijski sustavi - Visoka Skola Aspira 2018.

[SRC DIR](https://bitbucket.org/divukman/operacijskisustavi-vjezbe/src)

### [Operacijski sustavi - Vjezbe.pdf](https://bitbucket.org/divukman/operacijskisustavi-vjezbe/raw/771a2c2b3dfcb7658f41ffe14a0ec9aab5479478/Operacijski%20sustavi%20-%20Vje%C5%BEbe%201%20-%2010.pdf):
#### Sadrzaj:
1. Sto je Linux
2. Osnove Linuxa
3. Datotecni sustav
4. Procesi
5. Uvod u Linux programiranje - C
6. Prekidi i signali u Linuxu
7. Multithreading (Visedretvenost)
8. Suradnja procesa i kriticni odsjecci
9. Semafori
10. Raspberry Pi


### Source kod:
1. [Prekidi i signali](https://bitbucket.org/divukman/operacijskisustavi-vjezbe/src/54fcff7bc35d415e7ec98e6dc1a232dcb2a1904e/PREKIDI_I_SIGNALI/?at=master)
2. [Semafori](https://bitbucket.org/divukman/operacijskisustavi-vjezbe/src/54fcff7bc35d415e7ec98e6dc1a232dcb2a1904e/SEMAFORI/?at=master)
3. [Suradnja procesa i kriticni odsjecci](https://bitbucket.org/divukman/operacijskisustavi-vjezbe/src/54fcff7bc35d415e7ec98e6dc1a232dcb2a1904e/SURADNJA_PROCESA_I_KRITICNI_ODSJECCI/?at=master)
4. [Multithreading (Visedretvenost)](https://bitbucket.org/divukman/operacijskisustavi-vjezbe/src/54fcff7bc35d415e7ec98e6dc1a232dcb2a1904e/VISEDRETVENOST/?at=master)